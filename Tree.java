
/**
 * Create a Tree object that stores name,
 * min height, and max height. Methods to get
 * low, high, avg height, and name. Print to
 * string and check if int is in height range.
 * 
 * @author Kyle Hekhuis 
 * @version 10/11/2014
 */
public class Tree
{
    // Initialize string for name
    private String name;
    // Initialize Range object for range of heights
    private Range heightRange;
    
    /*
     * Creates Tree object with name and range of heights
     * with passed parameter of a name, min height, and 
     * max height.
     */
    public Tree(String pname,int plow,int phigh)
    {
        name = pname;
        heightRange = new Range(plow, phigh);
    }
    
    /*
     * Returns the low end of the range of heights for this tree.
     */
    public int getLow()
    {
        return heightRange.getLow();
    }
    
    /*
     * Returns the high end of the range of heights for this tree.
     */
    public int getHigh()
    {
        return heightRange.getHigh();
    }
    
    /*
     * Returns the name of the tree.
     */
    public String getName()
    {
        return name;
    }
    
    /*
     * Returns true if the integer parameter value is in the range
     * of typical heights for this kind of tree. Returns false otherwise.
     */
    public boolean inRange(int value)
    {
        return heightRange.inRange(value);
    }
    
    /*
     * Returns the concatenation of the name of the tree, 
     * the character tab (\t), the low end of the range, a 
     * second tab character (\t) and the high end of the range.
     */
    public String toString()
    {
        return name + "\t" + getLow() + "\t" + getHigh();
    }
    
    /*
     * Returns the average height of the tree.
     */
    public double getAverageHeight()
    {
        return heightRange.getAverage();
    }
}
