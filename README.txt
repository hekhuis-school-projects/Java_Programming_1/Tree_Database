Tree Database
Language: Java
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 163 - Java Programming 1
Semester / Year: Fall 2014

This program creates a simple tree database. It allows you to list
the trees in the database and make queries.

Usage:
Run the main method in 'GUI' to view the database and its options.
Types in your search in the input parameter then click the respective
button for what you're searching by.