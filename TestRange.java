import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class TestRange.
 *
 * @author  CIS162
 * @version Fall 2014
 */
public class TestRange
{
    /**
     * Default constructor for test class TestRange
     */
    public TestRange()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void testConstructor()
    {
        Range range1 = new Range(50, 80);
    }
    @Test
    public void testGetLow() 
    {
        Range range1 = new Range(50, 80);
        assertEquals(50, range1.getLow());
    }
    @Test
    public void testGetHigh() 
    {
        Range range1 = new Range(50, 80);
        assertEquals(80, range1.getHigh());
    }
    @Test
    public void testInRange() {
        Range range1 = new Range(50, 80);
        assertEquals(false, range1.inRange(45));
        assertEquals(true, range1.inRange(50));
        assertEquals(true, range1.inRange(65));
        assertEquals(true, range1.inRange(80));
        assertEquals(false, range1.inRange(100));
    }
    
}

