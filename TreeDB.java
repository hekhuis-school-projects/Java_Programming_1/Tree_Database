import java.util.ArrayList;

/**
 * Creates a database of trees with methods to
 * get min, max, and avg heights; query by name
 * and height; toString.
 *
 * @author Kyle Hekhuis
 * @version 10/11/2014
 */
public class TreeDB
{
    // Initialize ArrayList of Trees
    private ArrayList < Tree > listOfTrees;

    /**
     * Constructor for objects of class TreelistOfTrees
     */
    public TreeDB()
    {
        listOfTrees = new ArrayList < Tree >();

        Tree t1 = new Tree( "Pin Oak",      59, 72 );
        Tree t2 = new Tree( "White Pine",   50, 80 );
        Tree t3 = new Tree( "Silver Maple", 50, 80 );
        Tree t4 = new Tree( "Tulip Tree",   70, 90 );
        Tree t5 = new Tree( "River Birch",  40, 70 );

        listOfTrees.add( t1 );
        listOfTrees.add( t2 );
        listOfTrees.add( t3 );
        listOfTrees.add( t4 );
        listOfTrees.add( t5 );
    }

    /*
     * Turn the entire ArrayList into a formatted string.
     */
    public String toString() 
    {
        String result = "";
        
        for(int i = 0; i < listOfTrees.size(); i++)
        {
            String tempString = listOfTrees.get(i).toString();
            result = result + tempString + "\n";
        }
        
        return result;
    }

    /*
     * Search for and return trees matching name
     * of the passed string.
     */
    public String queryByName(String key) 
    {
        String result = "";
        
        for(int i = 0; i < listOfTrees.size(); i++)
        {
            String tempString = listOfTrees.get(i).toString();
            if( listOfTrees.get(i).getName().equals(key) )
            {
                result = result + tempString + "\n";
            }
        }
        
        return result;
    }

    /*
     * Search for and return trees whose range
     * includes the passed parameter.
     */
    public String queryByPossibleHeight(int key) 
    {
        String result = "";
        
        for(int i = 0; i < listOfTrees.size(); i++)
        {
            if( listOfTrees.get(i).inRange(key))
            {
                String tempString = listOfTrees.get(i).toString();
                result = result + tempString + "\n";
            }
        }
        
        return result;
    }

    /*
     * Return max height of all trees in
     * ArrayList.
     */
    public int getMaxHeight()
    {
        int max = 0;
        
        for(int i = 0; i < listOfTrees.size(); i++)
        {
            if( max < listOfTrees.get(i).getHigh() )
                max = listOfTrees.get(i).getHigh();
        }
        
        return max;
    }

    /*
     * Return min height of all trees in
     * ArrayList.
     */
    public int getMinHeight()
    {
        int min = listOfTrees.get(0).getLow();
        
        for(int i = 0; i < listOfTrees.size(); i++)
        {
            if( min > listOfTrees.get(i).getLow() )
                min = listOfTrees.get(i).getLow();
        }
        
        return min;
    }

    /*
     * Return avg height of all trees in
     * ArrayList.
     */
    public double getAverageHeight()
    {
        double total = 0;
        
        for(int i = 0; i < listOfTrees.size(); i++)
        {
            total += listOfTrees.get(i).getAverageHeight();
        }
        
        return total/listOfTrees.size();
    }
}
