/**
 * Creates a range object with a low and high value. 
 * Methods to get low, high and average of range.
 * Check if an int is in the range.
 * 
 * @author Kyle Hekhuis 
 * @version 10/11/2014
 */
public class Range
{
    // Initialize low and high integers
    private int low, high;
    
    /*
     * Constructs a range with low and high
     * parameters passed
     */
    public Range(int plow,int phigh)
    {
        low = plow;
        high = phigh;
    }
    
    /*
     * Returns the low end of the range
     */
    public int getLow()
    {
        return low;
    }
    
    /*
     * Returns the high end of the range
     */
    public int getHigh()
    {
        return high;
    }
    
    /*
     * Returns true if the integer parameter value is in the 
     * (closed) interval between low and high. 
     * Returns false otherwise.
     */
    public boolean inRange(int value) 
    {
        if(value >= low && value <= high) 
            return true;
        return false;
    }
    
    /*
     * Returns (low + high) * 0.5
     */
    public double getAverage()
    {
        double avg = ( low + high ) * .5;
        return avg;
    }
}
